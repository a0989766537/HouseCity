package tw.edu.ncku.housecityar;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * An activity that displays a map showing the place at the device's current location.
 */
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;

    private Menu mMenu;
    private int mLastSelectedItemId;
    private MenuItem mMyLocationMenuItem;
    private boolean mMyLocation;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    //private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    //private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_MY_LOCATION = "my_location";
    private static final String KEY_SELECTED_ITEM = "selected_Item";
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mMyLocation = savedInstanceState.getBoolean(KEY_MY_LOCATION);
            mLastSelectedItemId = savedInstanceState.getInt(KEY_SELECTED_ITEM);
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_maps);
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putBoolean(KEY_MY_LOCATION, mMyLocationMenuItem.isChecked());
            outState.putInt(KEY_SELECTED_ITEM, mLastSelectedItemId);
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.styled_map, menu);
        mMyLocationMenuItem = menu.findItem(R.id.my_location);
        mMyLocationMenuItem.setChecked(mMyLocation);
        // Turn on the My Location layer and the related control on the map.
        if (mMap != null) {
            if (mMyLocation)
                updateLocationUI(true);
            onOptionsItemSelected(mMenu.findItem(mLastSelectedItemId));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.my_location) {
            boolean isEnabled = !item.isChecked();
            if (isEnabled)
                if (checkLocationPermission())
                    if (LocationServiceHelper.statusCheck(this))
                        updateLocationUI(true);
                    else
                        LocationServiceHelper.buildAlertMessageNoGps(this);
                else
                    getLocationPermission();
            else
                updateLocationUI(false);
            return true;
        } else {
            int type = -1;
            switch (itemId) {
                case R.id.layer_normal_item:
                    type = GoogleMap.MAP_TYPE_NORMAL;
                    break;
                case R.id.layer_hybrid_item:
                    type = GoogleMap.MAP_TYPE_HYBRID;
                    break;
                case R.id.layer_satellite_item:
                    type = GoogleMap.MAP_TYPE_SATELLITE;
                    break;
                case R.id.layer_terrain_item:
                    type = GoogleMap.MAP_TYPE_TERRAIN;
                    break;
                case R.id.layer_none_item:
                    type = GoogleMap.MAP_TYPE_NONE;
                    break;
            }
            if (type != -1) {
                item.setChecked(true);
                mMap.setMapType(type);
                mMap.setMapStyle(null);
                mMenu.findItem(R.id.style_default_item).setChecked(true);
                mLastSelectedItemId = itemId;
                return true;
            }
            MapStyleOptions style;
            switch (itemId) {
                case R.id.style_retro_item:
                    // Sets the retro style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_retro);
                    break;
                case R.id.style_night_item:
                    // Sets the night style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_night);
                    break;
                case R.id.style_grayscale_item:
                    // Sets the grayscale style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_grayscale);
                    break;
                case R.id.style_no_pois_no_transit_item:
                    // Sets the no POIs or transit style via JSON string.
                    style = new MapStyleOptions("[" +
                            "  {" +
                            "    \"featureType\":\"poi.business\"," +
                            "    \"elementType\":\"all\"," +
                            "    \"stylers\":[" +
                            "      {" +
                            "        \"visibility\":\"off\"" +
                            "      }" +
                            "    ]" +
                            "  }," +
                            "  {" +
                            "    \"featureType\":\"transit\"," +
                            "    \"elementType\":\"all\"," +
                            "    \"stylers\":[" +
                            "      {" +
                            "        \"visibility\":\"off\"" +
                            "      }" +
                            "    ]" +
                            "  }" +
                            "]");
                    break;
                case R.id.style_default_item:
                    // Removes previously set style, by setting it to null.
                    style = null;
                    break;
                default:
                    return true;
            }
            if (mMap.setMapStyle(style)) {
                item.setChecked(true);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mMenu.findItem(R.id.layer_normal_item).setChecked(true);
                mLastSelectedItemId = itemId;
                return true;
            }
        }
        return false;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (mMenu != null) {
            if (mMyLocation)
                updateLocationUI(true);
            onOptionsItemSelected(mMenu.findItem(mLastSelectedItemId));
        }

        // Prompt the user for permission.
        //getLocationPermission();

        // Get the current location of the device and set the position of the map.
        if (mLastKnownLocation == null)
            getDeviceLocation();
        else {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())));
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
        }
    }

    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (checkLocationPermission()) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && (mLastKnownLocation = task.getResult()) != null) {
                            // Set the map's camera position to the current location of the device.
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            /*mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));*/
                            //mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private boolean checkLocationPermission() {
        return ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (checkLocationPermission()) {
            //mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        //mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //mLocationPermissionGranted = true;
                    onOptionsItemSelected(mMyLocationMenuItem);
                }
            }
        }
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI(boolean isEnabled) {
        if (!checkReady()) {
            return;
        }

        // Enable the location layer. Request the location permission if needed.
        if (checkLocationPermission() && isEnabled) {
            mMyLocationMenuItem.setChecked(true);
            mMap.setMyLocationEnabled(true);
        } else {
            // Uncheck the box until the layer has been enabled and request missing permission.
            mMyLocationMenuItem.setChecked(false);
            mMap.setMyLocationEnabled(false);
            getLocationPermission();
        }
    }

    public void snapshotMap(View view) {
        GoogleMap.SnapshotReadyCallback snapshotReadyCallback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap bitmap) {
                try {
                    //Write file
                    String filename = "bitmap.png";
                    FileOutputStream stream = MapsActivity.this.openFileOutput(filename, Context.MODE_PRIVATE);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    //Cleanup
                    stream.close();
                    bitmap.recycle();

                    //Pop intent
                    Intent intent = new Intent(MapsActivity.this, HelloSceneformActivity.class);
                    intent.putExtra("image",filename);
                    startActivity(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        mMap.snapshot(snapshotReadyCallback);
    }
}
