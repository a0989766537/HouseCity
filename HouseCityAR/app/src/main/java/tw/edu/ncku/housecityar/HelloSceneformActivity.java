/*
 * Copyright 2018 Google LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tw.edu.ncku.housecityar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Plane.Type;
import com.google.ar.core.Pose;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.RenderableDefinition;
import com.google.ar.sceneform.rendering.Texture;
import com.google.ar.sceneform.rendering.Vertex;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

/**
 * This is an example activity that uses the Sceneform UX package to make common AR tasks easier.
 */
public class HelloSceneformActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = HelloSceneformActivity.class.getSimpleName();

    private ArFragment arFragment;
    private ModelRenderable mapRenderable;
    private ModelRenderable houseRenderable;

    private DrawerLayout mDrawerLayout; //宣告點擊事件需要的變數
    private ImageView imageView;
    private MenuItem selectedItem;

    Anchor anchor;
    Button btn;
    int flag = 0;


    @Override
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    // CompletableFuture requires api level 24
    // FutureReturnValueIgnored is not valid
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ux);

        btn = (Button) findViewById(R.id.delete);
        btn.setOnClickListener(this);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

        try {
            String filename = getIntent().getStringExtra("image");
            setMapRenderable(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        mDrawerLayout = findViewById(R.id.drawer_layout); //點擊事件需要的原件在哪
        imageView = findViewById(R.id.imageView); //讀取imageview的指令

        // 為navigatin_view設置點擊事件
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    Timer closeTimer = new Timer();
                    int[] id_array = {R.id.build1, R.id.build2, R.id.build3, R.id.build4};
                    int[] raw_array = {R.raw.orange, R.raw.blue, R.raw.blue_rock, R.raw.red};
                    int[] drawable_array = {R.drawable.orange_icon, R.drawable.blue_icon, R.drawable.blue_rock_icon, R.drawable.red_icon};

                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        // set item as selected to persist highlight
                        selectedItem = menuItem;
                        menuItem.setChecked(true);
                        // 點選時收起選單
                        closeTimer.cancel();
                        closeTimer = new Timer();
                        closeTimer.schedule(new TimerTask(){
                            @Override
                            public void run() {
                                // close drawer when item is tapped
                                runOnUiThread(() -> mDrawerLayout.closeDrawers());
                            }
                        }, 5000);

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        //設定要用哪個3D模型
                        int rawID = 0;
                        for (int i = 0; i < id_array.length; i++)
                            if (menuItem.getItemId() == id_array[i]) {
                                rawID = raw_array[i];
                                imageView.setImageResource(drawable_array[i]);
                                break;
                            }

                        // When you build a Renderable, Sceneform loads its resources in the background while returning
                        // a CompletableFuture. Call thenAccept(), handle(), or check isDone() before calling get().
                        ModelRenderable.builder()
                                .setSource(HelloSceneformActivity.this, rawID)
                                .build()
                                .thenAccept(renderable -> houseRenderable = renderable)
                                .exceptionally(
                                        throwable -> {
                                            Toast toast =
                                                    Toast.makeText(HelloSceneformActivity.this, "Unable to load andy renderable", Toast.LENGTH_LONG);
                                            toast.setGravity(Gravity.CENTER, 0, 0 );
                                            toast.show();
                                            return null;
                                        });
                        return true;
                    }
                });




        arFragment.setOnTapArPlaneListener(
                (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {
                    ModelRenderable renderable = mapRenderable == null ? houseRenderable : mapRenderable;
                    if (renderable  == null) {
                        return;
                    }

                    if (plane.getType() != Type.HORIZONTAL_UPWARD_FACING) {
                        return;
                    }

                    // Create the Anchor.

                    if(mapRenderable!= null){
                        anchor = hitResult.getTrackable().createAnchor(hitResult.getHitPose().compose(Pose.makeTranslation(0, -0.05f, 0)));
                        }
                    else{ anchor = hitResult.getTrackable().createAnchor(hitResult.getHitPose().compose(Pose.makeTranslation(0, 0f, 0)));}

                    AnchorNode anchorNode = new AnchorNode(anchor);
                    anchorNode.setParent(arFragment.getArSceneView().getScene());


                    // Create the transformable andy and add it to the anchor.
                    TransformableNode andy = new TransformableNode(arFragment.getTransformationSystem());
                    andy.setParent(anchorNode);
                    andy.setRenderable(renderable);
                    andy.select();
                    flag =1 ;



                    if (flag == 1 && arFragment.getTransformationSystem().getSelectedNode().isSelected() == true ) {
                        btn.setVisibility(View.VISIBLE);
                        btn.setEnabled(true);
                    }    else {
                        btn.setVisibility(View.INVISIBLE);
                        btn.setEnabled(false);
                    }

                    if (renderable == mapRenderable) {
                        mapRenderable = null;
                        runOnUiThread(() -> mDrawerLayout.openDrawer(GravityCompat.START));
                    } else
                        deselectItem();

                });


    }


    private void deselectItem() {
        if (selectedItem != null) {
            houseRenderable = null;
            selectedItem.setChecked(false);
            imageView.setImageDrawable(null);
        }

    }

    private void setMapRenderable(String diffuseTextureFileName) throws FileNotFoundException {
        // Read the texture.
        FileInputStream is = this.openFileInput(diffuseTextureFileName);
        Bitmap textureBitmap = BitmapFactory.decodeStream(is);

        int width = textureBitmap.getWidth();
        int height = textureBitmap.getHeight();

        float increaseW = width > height ? ((float) width / height - 1) / 2 : 0;
        float increaseH = width < height ? ((float) height / width - 1) / 2 : 0;

        float[][] rectangleCoords = new float[][] {
                {0.5f + increaseW, 0,  0.5f + increaseH, 0.0f},  // top right
                {0.5f + increaseW, 0, -0.5f - increaseH, 0.0f},  // bottom right
                {-0.5f - increaseW, 0, -0.5f - increaseH, 0.0f}, // bottom left
                {-0.5f - increaseW, 0, 0.5f + increaseH, 0.0f},  // top left
        };
        final float[][] planeTextureCoordinateData =
                {
                        {1, 0},
                        {1, 1},
                        {0, 1},
                        {0, 0}
                };

        // Vertices
        ArrayList<Vertex> vertices = new ArrayList<>();
        for (int i = 0; i < rectangleCoords.length; i++)
            vertices.add(Vertex.builder().setPosition(new Vector3(rectangleCoords[i][0], rectangleCoords[i][1], rectangleCoords[i][2])).setUvCoordinate(new Vertex.UvCoordinate(planeTextureCoordinateData[i][0], planeTextureCoordinateData[i][1]))/*.setColor(new Color(1, 1, 1, 1))*/.build());

        // Submeshes
        ArrayList<RenderableDefinition.Submesh> submeshes = new ArrayList<>();
        final Texture[] texture = new Texture[1];
        Callable<InputStream> inputStreamCreator = () -> {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            textureBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            return new ByteArrayInputStream(bitmapdata);
        };
        Texture.builder().setSource(inputStreamCreator).build().thenAccept(t -> texture[0] = t);
        //Texture.builder().setSource(this, R.drawable.ic_launcher_background).build().thenAccept(t -> texture[0] = t);
        //final Material[] material = new Material[1];


        MaterialFactory.makeOpaqueWithTexture(this, texture[0]).thenAccept((material) -> {

            RenderableDefinition.Submesh submesh = RenderableDefinition.Submesh.builder()
                    .setMaterial(material)
                    .setTriangleIndices(Arrays.asList(0, 1, 2, 0, 2, 3)).build();
            submeshes.add(submesh);

            // RenderableDefinition
            RenderableDefinition renderableDefinition = RenderableDefinition.builder()
                    .setVertices(vertices)
                    .setSubmeshes(submeshes)
                    .build();

            // When you build a Renderable, Sceneform loads its resources in the background while returning
            // a CompletableFuture. Call thenAccept(), handle(), or check isDone() before calling get().
            ModelRenderable.builder()
                    //.setSource(this, R.raw.andy)
                    .setSource(renderableDefinition)
                    .build()
                    .thenAccept(renderable -> mapRenderable = renderable)
                    .exceptionally(
                            throwable -> {
                                Toast toast =
                                        Toast.makeText(this, "Unable to load andy renderable", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return null;
                            });

        });

    }


    @Override
    public void onClick(View v) {
            TransformableNode Select_One =  arFragment.getTransformationSystem().getSelectedNode();
            ((AnchorNode)Select_One.getParent()).getAnchor().detach();
    }
}
